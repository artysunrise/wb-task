import React from 'react';
import { Link } from 'react-router';

const Header = (props) => {
  return (
    <header className="clearfix">
      <Link to="home">Weather</Link>
      <nav className="clearfix">
        <div className="nav-item">
          <Link to="home">Home</Link>
        </div>
      </nav>
    </header>
  )
};

export default Header;
