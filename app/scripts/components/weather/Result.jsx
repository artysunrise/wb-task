import React from 'react';
import WeatherNow from "./Now.jsx";
import Forecast from "./Forecast.jsx";

const WeatherResult = ({loading, weatherData, fetchError}) => {
  if (loading) {
    return <div>Loading...</div>;
  }

  if (fetchError) {
    return <div>{fetchError}</div>;
  }

  if (!weatherData || !weatherData.location) {
    return <span />;
  }

  const {localtime} = weatherData.location;

  return (
    <div>
      <WeatherNow {...weatherData} />
      <Forecast {...weatherData} />

      <div className="text-muted text-center">
        <small className="text-muted">Weather for {localtime} (localtime) by
          <a href="http://apixu.com" target="_blank"> apixu.com API</a>
        </small>
      </div>
    </div>
  )
};

WeatherResult.propTypes = {
  loading : React.PropTypes.bool,
  weatherData : React.PropTypes.object,
  fetchError : React.PropTypes.string,
}

export default WeatherResult;
