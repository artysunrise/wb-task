import React from 'react';

const ForecastDay = (props) => {
  const {date, day: {avgtemp_c,
      condition: {icon, text: c_text},
    },
  } = props;

  return (
    <div className="f-day">
      <div className="date">{date}</div>
      <h3>{avgtemp_c}°</h3>
      <div>
        <img src={icon} alt={c_text} />
      </div>
    </div>
  )
}

const WeatherForecast = ({forecast}) => {

  return (
    <div>
      <h2>Forecast for next 7 days</h2>
      <div className="forecast-block">
        {
          forecast.forecastday.map(item => (
            <ForecastDay key={item.date_epoch} {...item} />
          ))
        }
      </div>
    </div>
  )
};

WeatherForecast.propTypes = {
  forecast : React.PropTypes.object,
}

export default WeatherForecast;
