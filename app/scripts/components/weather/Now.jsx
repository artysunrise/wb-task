import React from 'react';

const WeatherNow = ({location, current}) => {
  const {name, country, localtime} = location;
  const {humidity, pressure_mb, temp_c, condition: {icon, text}} = current;

  return (
    <div>
      <h2>Now in {country}, {name}:</h2>
      <div className="weather-result-block">
        <div>
          <img src={icon} alt={text} />
        </div>
        <h2>{temp_c}°</h2>
        <div>
          <p className="text-muted"><strong>Humidity</strong>: {humidity} %</p>
          <p className="text-muted"><strong>Pressure</strong>: {pressure_mb} millibars</p>
        </div>
      </div>
    </div>
  )
};

WeatherNow.propTypes = {
  weatherData : React.PropTypes.object,
}

export default WeatherNow;
