import React from 'react';

const WeatherForm = ({currentCityEntered, formInvalid, loading, actions}) => {
  const fetchByClick = (cityName, event) => {
    event.preventDefault();

    if (loading) {
      return false;
    }

    actions.fetchWeather(cityName);
  }

  return (
    <div>
      <form className="pure-form"
        onSubmit={e => {actions.fetchWeather(currentCityEntered); e.preventDefault();}}>
        <fieldset disabled={loading}>
          <div className="input-block">
            <input type="text" placeholder="Enter city name"
              value={currentCityEntered}
              onChange={e => actions.cityChanged(e.target.value)}/>
            <button disabled={formInvalid || loading} className="pure-button pure-button-primary">Get</button>
          </div>

        </fieldset>
        <div className="examples-block text-muted">
          For example:
          <a className="pseudo-link" href="#" onClick={fetchByClick.bind(null, "Moscow")}>Moscow,</a>
          <a className="pseudo-link" href="#" onClick={fetchByClick.bind(null, "Ekaterinburg")}>Ekaterinburg</a> or
          <a className="pseudo-link" href="#" onClick={fetchByClick.bind(null, "Limassol")}>Limassol</a>
        </div>
      </form>
    </div>
  )
};

WeatherForm.propTypes = {
  loading : React.PropTypes.bool,
  formInvalid : React.PropTypes.bool,
  currentCityEntered : React.PropTypes.string,
  actions : React.PropTypes.object,
}

export default WeatherForm;
