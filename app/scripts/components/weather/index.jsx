import React from 'react';
import Form from "./Form.jsx";
import Result from "./Result.jsx";

const Weather = (props) => {
  return (
    <div className="weather-page">
      <Form {...props} />
      <Result {...props} />
    </div>
  )
};

export default Weather;
