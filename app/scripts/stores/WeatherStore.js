/**
 * WeatherStore
 *
 * @exports WeatherStore singleton
 */

import AbstractStore from '../stores/AbstractStore';

/**
 * @inheritdoc
 */
export default class WeatherStore extends AbstractStore {
  /**
   * @inheritdoc
   */
  getActionListeners() {
    return {
      /**
       * input city name changes
       */
      'city:changed': ({name, formInvalid}) => {
        this
          .set('currentCityEntered', name)
          .set('formInvalid', formInvalid)
          .trigger('change');
      },

      'city:fetch:start': (cityName) => {
        this
          .set('loading', true)
          .set('currentCityEntered', cityName)
          .set('weatherData', {})
          .set('fetchError', "")
          .trigger('change');
      },

      'city:fetch:end': () => {
        this
          .set('loading', false)
          .trigger('change');
      },

      'city:fetch:success': (weatherData) => {
        this
          .set('weatherData', weatherData)
          .trigger('change');
      },

      'city:fetch:error': (fetchError) => {
        this
          .set('fetchError', fetchError)
          .trigger('change');
      },
    };
  }
}

const instance = new WeatherStore();

export default instance;
