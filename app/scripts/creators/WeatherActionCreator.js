/**
 * Weather page actions
 *
 * @exports WeatherActionCreator singleton
 */

import AbstractActionCreator from '../creators/AbstractActionCreator';
import Fetch from "../utils/Fetch";

// хранить так открыто кеи, конечно, нельзя :)
const API_KEY = "8c151d701e6944bb82f190041161908";

class WeatherActionCreator extends AbstractActionCreator {
  cityChanged(name) {
    let formInvalid = false;

    if (name.length === 0) {
      formInvalid = true;
    }

    this.dispatch('city:changed', {name, formInvalid});
  }

  fetchWeather(cityName) {
    const url = `http://api.apixu.com/v1/forecast.json?key=${API_KEY}&q=${cityName}&days=7`;

    this.dispatch('city:fetch:start', cityName);

    Fetch(url)
      .then((data) => {
        if (data.error) {
          this.dispatch('city:fetch:error', data.error.message);
          return;
        }

        this.dispatch('city:fetch:success', data);
      })
      .catch((err) => this.dispatch('city:fetch:error', "Network error"))
      .then((err) => this.dispatch('city:fetch:end'));
  }
}

const instance = new WeatherActionCreator();

export default instance;
