import React from 'react';
import AbstractComponent from '../components/AbstractComponent.jsx';
import Weather from '../components/weather/index.jsx';
import WeatherStore from '../stores/WeatherStore';
import WeatherActionCreator from '../creators/WeatherActionCreator';

class Home extends AbstractComponent {
  /**
   * @inheritdoc
   */
  constructor(props){
    super(props);

    this.state = {
      loading: false,
      currentCityEntered: "",
      formInvalid: true,
      weatherData: {},
      fetchError: "",
    };
  }

  /**
   * @inheritdoc
   */
  getStoresConfig() {
    return [
      {
        store: WeatherStore,
        eventName: 'change',
        callback: this.storeChangeHandler.bind(this),
      },
    ];
  }

  /**
   * Weather store change handler
   */
  storeChangeHandler() {
    // TODO: нужно сделать возврат сразу всего store, а не по отдельности
    this.setState({
      loading: WeatherStore.get('loading'),
      currentCityEntered: WeatherStore.get('currentCityEntered'),
      formInvalid: WeatherStore.get('formInvalid'),
      weatherData: WeatherStore.get('weatherData'),
      fetchError: WeatherStore.get('fetchError'),
    });
  }

  /**
   * @inheritdoc
   */
  componentDidMount() {
    super.componentDidMount();
  }

  /**
   * @inheritdoc
   */
  render() {
    return (
      <div>
        <Weather {...this.state} actions={WeatherActionCreator} />
      </div>
    );
  }
}

export default Home;
