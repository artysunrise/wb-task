import "whatwg-fetch";

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export default function Fetch(url, options = {}) {
  return new Promise((res, rej) => {
    window.fetch(url)
      .then(checkStatus)
      .then((response) => response.json())
      .then((json) => res(json))
      .catch((err) => rej(err));
  });
}
